import javax.swing.*;

public class DtoE {

    public static void getValue()
    {

        double dollar = Double.parseDouble(JOptionPane.showInputDialog("Enter the value in Dollar you wish to convert."));

        double euro = convert(dollar);

        JOptionPane.showMessageDialog(null,
                "The equivalent in Euro is: " + String.format("%.2f", euro),
                "Dollar to Euro", JOptionPane.INFORMATION_MESSAGE);

        Converter.userChoice();

    }

    private static double convert(double dollar) {

        return dollar * 0.847862;

    }

}
