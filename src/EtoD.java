import javax.swing.*;

public class EtoD {

    public static void getValue()
    {

        double euro = Double.parseDouble(JOptionPane.showInputDialog("Enter the value in Euro you wish to convert."));

        double dollar = convert(euro);

        JOptionPane.showMessageDialog(null,
                "The equivalent in Dollars is: " + String.format("%.2f", dollar),
                "Euro to Dollar", JOptionPane.INFORMATION_MESSAGE);

        Converter.userChoice();

    }

    private static double convert(double euro) {

        return euro * 1.1794372197;

    }

}
