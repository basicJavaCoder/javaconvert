import javax.swing.*;

public class Goodbye {

    public static void sayBye() {

        JOptionPane.showMessageDialog(null,"Thank you for using my Converter!", "Converter", JOptionPane.INFORMATION_MESSAGE);

        System.exit(0);
    }

}
