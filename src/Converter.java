import javax.swing.*;

public class Converter {

    public static void main(String[] args) {

        userChoice();

    }

    public static void userChoice() {

        String mainMenu = "Please select from one of the options below by typing the corresponding number:\n\n";
        String options = "1) Euro to Dollar\n2) Dollar to Euro\n3) Mile to Kilometres\n4) Kilometres to Miles\n5) Quit\n\n";

        int userSelection = Integer.parseInt(JOptionPane.showInputDialog(mainMenu + options));

        runSelection(userSelection);

    }

    private static void runSelection(int userSelection) {

        switch (userSelection) {
            case 1 -> EtoD.getValue();
            case 2 -> DtoE.getValue();
            case 3 -> MtoK.getValue();
            case 4 -> KtoM.getValue();
            case 5 -> Goodbye.sayBye();
        }

    }

}
