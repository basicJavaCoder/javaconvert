import javax.swing.*;

public class KtoM {

    public static void getValue()
    {

        double km = Double.parseDouble(JOptionPane.showInputDialog("Enter the amount of kilometers you wish to convert to miles"));

        double miles = convert(km);

        JOptionPane.showMessageDialog(null,
                miles + " km is equal to " + String.format("%.2f", miles) + " mi.",
                "Kilometers to Miles", JOptionPane.INFORMATION_MESSAGE);

        Converter.userChoice();

    }

    private static double convert(double km) {

        return km * 0.62137;

    }

}
