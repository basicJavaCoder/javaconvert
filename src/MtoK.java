import javax.swing.*;

public class MtoK {

    public static void getValue()
    {

        double miles = Double.parseDouble(JOptionPane.showInputDialog("Enter the amount of miles you wish to convert to Kilometers"));

        double km = convert(miles);

        JOptionPane.showMessageDialog(null,
                miles + " miles is equal to " + String.format("%.2f", km) + " km.",
                "Miles to Kilometers", JOptionPane.INFORMATION_MESSAGE);

        Converter.userChoice();

    }

    private static double convert(double miles) {

        return miles / 0.62137;

    }

}
